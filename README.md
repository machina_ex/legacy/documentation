Plaitools Documentation
=======================

Plaitools plaiframe documentation is hostet using [gitlab pages](https://docs.gitlab.com/ee/user/project/pages/) and split into three parts.


[Server documentation](https://machina_ex.gitlab.io/legacy/documentation/plaiframe/) includes the core part of the plaiframe backend.

[Plugin documentation](https://machina_ex.gitlab.io/legacy/documentation/plaiframe/plugins/) includes documentation for each plugin and the plugin boilerplates. Also a more detailed document on how to build plugins for plaiframe.

[webview code documentation](https://machina_ex.gitlab.io/legacy/documentation/plaiframe/webview/) includes frontend editor and control web apps and socket interface to the server application.

It's created with [JSDoc](https://github.com/jsdoc/jsdoc) from source code comments.

How to document
----------------

Take a look into how to [use JSDoc](https://jsdoc.app/).

Update documentation
-----------------------

To update this documentation, you need to clone this repository using the same directory plaiframe is in. Then cd into `./documentation` and install.

`$ npm install`

Now whenever there are changes in source code comments in plaiframe server, webview or a plugin do

`sh create.sh`

When done, push changes to documentation repository.

