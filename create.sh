alias npm-exec='PATH=$(npm bin):$PATH'

npm-exec jsdoc -c plaiframe_jsdoc_config.json
npm-exec jsdoc -c plaiframe_plugins_jsdoc_config.json
npm-exec jsdoc -c plaiframe_webview_jsdoc_config.json