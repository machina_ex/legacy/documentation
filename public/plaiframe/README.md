Plaiframe Documentation
===============================

Welcome to the Plaitools framework source code documentation. 

This is an auto generated website created with [JSDocs](https://jsdoc.app/).

All Code descriptions are generated from comments off the [plaiframe project](https://gitlab.com/plaitools/plaiframe).

Plaitools plaiframe documentation is split into three parts:

[Server documentation](https://plaitools.gitlab.io/documentation/plaiframe/) includes the core part of the plaiframe backend.

[Plugin documentation](https://plaitools.gitlab.io/documentation/plaiframe/plugins/) includes documentation for each plugin and the plugin boilerplates. Also a more detailed description on how to build plugins for plaiframe.

[webview code documentation](https://plaitools.gitlab.io/documentation/plaiframe/webview/) includes frontend editor and control web apps and socket interface to the server application.

Copyright 2020 Lasse Marburg, Benedikt Kaffai

plaiframe is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

plaiframe is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
